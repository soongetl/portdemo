# portdemo

The Soong demos ported to PortPHP for comparison.

## Install

Via Composer

``` bash
$ composer require soongetl/portdemo
```

## Demos

To setup for demoing PortPHP:

1. Create an empty database for testing.
1. Import data/extractsource.sql to the database (table to be populated by the first demo).
1. Import data/beer.sql to the database (tables to be populated for the second demo).
1. Edit ? - where indicated, replace the sample credentials with those for the test database.

Demo 1:

1. Execute `bin/port migrate arraytosql`
1. Look at the `extractsource` table to see the data populated.
1. ? Look at the `map_arraytosql` table to see the mapping from source to destination keys (identical in this case).
1. Execute `bin/port migrate sqltocsv`
1. Observe CSV data output to the terminal with configured transformations applied.

Demo 2:

1. Execute `bin/port migrate beertopics`
1. Observe the `beer_terms` table is populated from CSV data - in particular, see how the 'red ale' reference to its 'ale' parent has been converted to the numeric ID assigned to the 'ale' row in the database.
1. Execute `bin/port migrate beeraccounts`
1. Observe the `beer_users` table - in particular, see how (?) converted the boolean values in the `pro` column to strings in the `taster` column.
1. Execute `bin/port migrate beercontent`
1. Observe the `beer` table - in particular, see how the relationships to users/accounts was maintained even though the IDs for the users changed (also see the `map_beeraccounts` table).
1. Execute `bin/port rollback beercontent`
1. Observe how the `beer` and `map_beercontent` tables are now empty.

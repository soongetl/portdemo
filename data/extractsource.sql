DROP TABLE IF EXISTS `extractsource`;
CREATE TABLE `extractsource` (
  `uniqueid` int(11) NOT NULL,
  `foo` varchar(255) NOT NULL,
  `bar` varchar(255) NOT NULL,
  `num` int(11) NOT NULL,
  `related` int(11) DEFAULT NULL,
  PRIMARY KEY (`uniqueid`)
);

<?php
declare(strict_types=1);

namespace Port\Task;

/**
 * Basic base class for migration tasks.
 *
 * @package Port\Task
 */
class Task implements TaskInterface
{
    /**
     * Configuration for the task.
     *
     * @var array
     *   Configuration values keyed by configuration name.
     */
    protected $configuration = [];

    /**
     * All known tasks, keyed by id.
     *
     * @var \Port\Task\TaskInterface[]
     */
    static protected $tasks = [];

    /**
     * Save the configuration.
     *
     * @param array $configuration
     *   Configuration values keyed by configuration name.
     */
    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(string $operation, array $options = [])
    {
        if (method_exists($this, $operation)) {
            $this->$operation($options);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function addTask(string $id, array $configuration)
    {
        static::$tasks[$id] = new static($configuration);
    }

    /**
     * {@inheritdoc}
     */
    public static function getTask(string $id): TaskInterface
    {
        return static::$tasks[$id];
    }

    /**
     * {@inheritdoc}
     */
    public static function getAllTasks(): array
    {
        return static::$tasks;
    }
}

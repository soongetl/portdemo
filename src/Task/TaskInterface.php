<?php
declare(strict_types=1);

namespace Port\Task;

/**
 * Interface with tasks which may be part of a migration pipeline.
 *
 * @package Port\Task
 */
interface TaskInterface
{

    /**
     * Execute the named operation for the task.
     *
     * @param string $operation
     *   Name of the operation, which is a method on the class.
     * @todo Do better than to just name a method here.     *
     * @param array $options
     *   List of options affecting execution of the operation.
     *
     * @return mixed
     * @todo Should return some sort of status
     */
    public function execute(string $operation, array $options = []);

    /**
     * Return the task's configuration as an array.
     *
     * @return array
     */
    public function getConfiguration() : array;

    /**
     * Retrieve the specified task.
     *
     * @param $id
     *
     * @return \Port\Task\TaskInterface
     */
    public static function getTask(string $id) : TaskInterface;

    /**
     * Retrieve a list of all tasks, keyed by ID.
     *
     * @return TaskInterface[]
     */
    public static function getAllTasks() : array;

    /**
     * Create a task object with a given id from provided configuration.
     *
     * @param string $id
     * @param array $configuration
     *
     * @return null
     */
    public static function addTask(string $id, array $configuration);
}

<?php
declare(strict_types=1);

namespace Port\Task;

use Port\Exception;
use Port\Steps\Step\MappingStep;
use Port\Steps\Step\ValueConverterStep;
use Port\Steps\StepAggregator;

/**
 * Implementation of operations for a full ETL process.
 *
 * @package Port\Task
 */
class EtlTask extends Task implements EtlTaskInterface
{

    /**
     * Perform an ETL migration operation.
     *
     * @param array $options
     */
    public function migrate(array $options)
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['reader'])) {
            return;
        }
        // Readers each have distinct construction signatures.
        if (($readerClass = $taskConfiguration['reader']['class']) == 'Port\Csv\CsvReader') {
            $file = new \SplFileObject($taskConfiguration['reader']['configuration']['csv_file_path']);
            /** @var \Port\Csv\CsvReader $reader */
            $reader = new $readerClass($file);
            $reader->setHeaderRowNumber(0);
        }

        if (($writerClass = $taskConfiguration['writer']['class']) == 'Port\Doctrine\DoctrineWriter') {
            /** @var \Port\Writer $writer */
            $reader = new $writerClass($file);
        }

        $writer = new $taskConfiguration['writer']['class']($taskConfiguration['writer']['configuration']);

        // MappingStep keys mappings by source, while we are destination-focused.
        $mappingStep = new MappingStep();
        foreach ($taskConfiguration['mapping'] as $key => $val) {
            $mappingStep->map("[$val]", "[$key]");
        }

        $converterStep = new ValueConverterStep();
        foreach ($taskConfiguration['converter'] as $destinationField => $converter) {
            $converterStep->add("[$destinationField]", new $converter['class']($converter['configuration']));
        }

        $workflow = new StepAggregator($reader, "A task");
        $workflow->setSkipItemOnFailure(false);
        $workflow->addWriter($writer);
        $workflow->addStep($mappingStep);
        $workflow->addStep($converterStep);
        try {
            $result = $workflow->process();
            print (sprintf('Processed %d successes and %d errors in %d microseconds.', $result->getSuccessCount(),
              $result->getErrorCount(), $result->getElapsed()->format('f')));
        }
        catch (Exception $e) {
            print $e->getMessage();
        }

    }

}

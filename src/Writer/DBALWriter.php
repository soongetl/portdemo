<?php

namespace Port\Writer;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Port\Writer;

/**
 * A DBAL writer
 *
 * @author David de Boer <david@ddeboer.nl>
 */
class DBALWriter implements Writer
{

    /**
     * @var array
     */
    protected $configuration = [];

    /**
     * The DBAL Connection.
     *
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    public function __construct(array $configuration) {
        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        if (empty($this->connection)) {
            try {
                $this->connection = DriverManager::getConnection($this->configuration['connection']);
            } catch (DBALException $e) {
                print $e->getMessage();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function finish()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        try {
            // Hack, no way to remove unused "beers" source column.
            $this->connection->insert(
              $this->configuration['table'],
              $item
            );
        } catch (DBALException $e) {
            print $e->getMessage();
        }
    }

}

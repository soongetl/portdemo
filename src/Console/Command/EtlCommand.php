<?php
declare(strict_types=1);

namespace Port\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Yaml\Yaml;

/**
 * Base class for all PortPHP console commands.
 *
 * @package Soong\Console\Command
 */
class EtlCommand extends Command
{

    /**
     * Console argument - one or more task IDs.
     *
     * @return \Symfony\Component\Console\Input\InputArgument
     */
    protected function tasksArgument(int $required = InputArgument::REQUIRED)
    {
        return new InputArgument(
            'tasks',
            InputArgument::IS_ARRAY | $required,
            'List of task IDs to process'
        );
    }

    /**
     * Console option - one or more directories containing YAML configuration.
     *
     * @return \Symfony\Component\Console\Input\InputOption
     */
    protected function directoryOption()
    {
        return new InputOption(
          'directory',
          null,
          InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
          'List of directories containing tasks to migrate',
          ['config']
        );
    }

    /**
     * Obtain all task configuration contained in the specified directories.
     *
     * @param array $directoryNames
     *   List of directories containing task configuration.
     */
    protected function loadConfiguration(array $directoryNames)
    {
        foreach ($directoryNames as $directoryName) {
            $directory = new \RecursiveDirectoryIterator(
              $directoryName,
              \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS
            );
            $filter = new YamlRecursiveFilterIterator($directory);
            $allFiles = new \RecursiveIteratorIterator(
              $filter,
              \RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($allFiles as $file) {
                if (!$file->isDir()) {
                    $configuration = Yaml::parse(file_get_contents($file->getPathname()));
                    foreach ($configuration as $id => $taskConfiguration) {
                        if ($id == 'beeraccounts') {
                            /** @var \Port\Task\Task $taskClass */
                            $taskClass = $taskConfiguration['class'];
                            $taskClass::addTask($id,
                              $taskConfiguration['configuration']);
                        }
                    }
                }
            }
        }
    }
}

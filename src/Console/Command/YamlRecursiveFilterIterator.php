<?php
declare(strict_types=1);

namespace Port\Console\Command;

/**
 * Gather all YAML files in a directory.
 *
 * @package Soong\Console\Command
 */
class YamlRecursiveFilterIterator extends \RecursiveFilterIterator
{

    /**
     * {@inheritdoc}
     */
    public function accept()
    {
        $file = parent::current();
        if ($file->isDir()) {
            return true;
        }
        $name = $file->getFilename();
        return (substr($name, -4) == '.yml' || substr($name, -5) == '.yaml');
    }
}

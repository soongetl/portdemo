<?php
declare(strict_types=1);

namespace Port\Console\Command;

use Port\Task\Task;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateCommand extends EtlCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("migrate")
          ->setDescription("Migrate data from one place to another")
          ->setDefinition([
            $this->tasksArgument(),
            $this->directoryOption(),
          ])
          ->setHelp(<<<EOT
The <info>migrate</info> command does things and stuff
EOT
          );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directoryNames = $input->getOption('directory');
        $this->loadConfiguration($directoryNames);
        foreach ($input->getArgument('tasks') as $id) {
            if ($task = Task::getTask($id)) {
                $output->writeln("<info>Executing $id</info>");
                $task->execute('migrate');
            } else {
                $output->writeln("<error>$id not found</error>");
            }
        }
    }
}

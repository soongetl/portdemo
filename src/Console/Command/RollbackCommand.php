<?php
declare(strict_types=1);

namespace Port\Console\Command;

use Port\Task;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RollbackCommand extends EtlCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("rollback")
          ->setDescription("Remove migrated data")
          ->setDefinition([
            $this->tasksArgument(),
            $this->directoryOption(),
          ])
          ->setHelp(<<<EOT
The <info>rollback</info> command does things and stuff
EOT
          );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directoryNames = $input->getOption('directory');
        $this->loadConfiguration($directoryNames);
        foreach ($input->getArgument('tasks') as $id) {
            // @todo: Hard-coded Task implementation
            if ($task = Task::getTask($id)) {
                $output->writeln("<info>Executing $id</info>");
                $task->execute('rollback');
            } else {
                $output->writeln("<error>$id not found</error>");
            }
        }
    }
}
